﻿using System;
using System.Collections.Generic;
namespace lyrical_engine_xna
{
    // ----------------------------------------------------------------------------------
    /*! \brief 
     * Calls functions on Systems and Managers
     * \n
     * システムとマネジャーの関数を呼び出し
     */
    // ----------------------------------------------------------------------------------
    public class World : IUpdatable, IFixedUpdatable, IDrawable
    {
        /* ----------------------------------------------------------------------------------
         * CLASS DECLARATION
         * ---------------------------------------------------------------------------------- */
        private EntityManager entManager;
        public static EntityManager entManagerReference;
        private AnimationSystem animationSystem;
        private InputSystem inputSystem;

        /* ----------------------------------------------------------------------------------
         * FIELDS & PROPERTIES
         * ---------------------------------------------------------------------------------- */


        /* ----------------------------------------------------------------------------------
         * CONSTRUCTOR
         * ---------------------------------------------------------------------------------- */
        public World()
        {
            entManager = new EntityManager();
            animationSystem = new AnimationSystem();
            inputSystem = new InputSystem();

            entManagerReference = entManager;
        }
        /* ----------------------------------------------------------------------------------
         * FUNCTIONS & STUFF
         * ---------------------------------------------------------------------------------- */
        // Call UpdateObjects on all systems
        public void Update()
        {
            inputSystem.Update();
        }

        // Call FixedUpdateObjects() on all systems
        public void FixedUpdate()
        {
            animationSystem.FixedUpdate();
        }

        // Call Draw() on all systems
        public void Draw()
        {
            animationSystem.Draw();
        }
    }
}
