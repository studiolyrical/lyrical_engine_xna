﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lyrical_engine_xna
{
    public class InputSystem : ISystem, IUpdatable
    {

        private ICommand keyA = new SpeakCommand();
        private ICommand nullCommand = new NullCommand();
        private ICommand doStuff;

        // ----------------------------------------- TEMP CODE -----------------------------------------
        Microsoft.Xna.Framework.Input.KeyboardState oldkbstate;
        // ----------------------------------------- TEMP CODE -----------------------------------------

        public void Update()
        {
            HandleInput();
            doStuff = HandleInput();

            if (doStuff != null)
            {
                doStuff.Execute(ent);
            }
        }

        private ICommand HandleInput()
        {
            // ----------------------------------------- TEMP CODE -----------------------------------------
            Microsoft.Xna.Framework.Input.KeyboardState kbstate = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            if (oldkbstate.IsKeyUp(Microsoft.Xna.Framework.Input.Keys.A) && kbstate.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.A))
            {
                oldkbstate = kbstate;
                return keyA;
            }
            else
            {
                oldkbstate = kbstate;
                return nullCommand;
            }

            //if (oldkbstate.IsKeyUp(Microsoft.Xna.Framework.Input.Keys.X) && kbstate.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.X))
            //{
            //    World.entManagerReference.FetchEntity(100, new Microsoft.Xna.Framework.Vector2(5.0f, 5.0f));
            //}

            //if (oldkbstate.IsKeyUp(Microsoft.Xna.Framework.Input.Keys.Z) && kbstate.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Z))
            //{
            //    World.entManagerReference.FetchEntity(200, new Microsoft.Xna.Framework.Vector2(10.0f, 10.0f));
            //}


            // ----------------------------------------- TEMP CODE -----------------------------------------
        }


    }
}
