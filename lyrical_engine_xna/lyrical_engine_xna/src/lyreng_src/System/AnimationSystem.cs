﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace lyrical_engine_xna
{
    // ----------------------------------------------------------------------------------
    /*! \brief 
     * Provide it a TextureAtlas, then this system will iterate through the "frames" 
     * of the atlas to create an animation 
     * \n 
     * 日本語でおｋ
     * 
     * \todo ● Support SPINE Runtimes (create another system?)
     * \todo ● IF tex == currentTex THEN currentFrame == 0
     */ 
    // ----------------------------------------------------------------------------------
    public class AnimationSystem : ISystem, IFixedUpdatable, IDrawable
    {
        /* ----------------------------------------------------------------------------------
         * FIELDS & PROPERTIES
         * ---------------------------------------------------------------------------------- */
        // Field for ghetto sample rate loop
        private const float DELTA_TIME = 100.0f;


        /* ----------------------------------------------------------------------------------
         * FUNCTIONS & STUFF
         * ---------------------------------------------------------------------------------- */
        public void FixedUpdate()
        {
            FrameUpdate();
        }

        /* ----------------------------------------------------------------------------------
         * ● FrameUpdate() runs at an arbitrarily defined "sample rate" to define the speed
         * of an animation.
         * ● The logic is very similar to the main fixedupdate loop in Game1.cs
         * ---------------------------------------------------------------------------------- */
        private void FrameUpdate()
        {
            foreach (Entity ent in EntityManager.ActiveEntities)
            {
                ent.Animator.Accumulator += ent.Animator.SampleRate;
                while (ent.Animator.Accumulator >= DELTA_TIME) // Step through frames based on SampleRate
                {
                    ent.Animator.CurrentFrame++; // Go to next frame
                    if (ent.Animator.CurrentFrame == ent.Animator.TotalFrames)
                        ent.Animator.CurrentFrame = 0;
                    ent.Animator.T += DELTA_TIME;
                    ent.Animator.Accumulator -= DELTA_TIME;
                }
            }
        }

        public void Draw()
        {
            foreach (Entity ent in EntityManager.ActiveEntities)
            {
                // ○Texture ○Animator
                if (ent.Animator != null && ent.Texture != null)
                {
                    Texture2D tex = Game1.spriteContent[ent.Texture.Sprite];
                    // Lifted from rbwhitaker: http://rbwhitaker.wikidot.com/texture-atlases-2
                    int width = tex.Width / ent.Texture.Columns;
                    int height = tex.Height / ent.Texture.Rows;
                    int row = (int)((float)ent.Animator.CurrentFrame / (float)ent.Texture.Columns);
                    int column = ent.Animator.CurrentFrame % ent.Texture.Columns;

                    Rectangle sourceRectangle = new Rectangle(width * column, height * row, width, height);
                    Rectangle destinationRectangle = new Rectangle((int)ent.C.Position.X, (int)ent.C.Position.Y, width, height);
                    Game1.spriteBatchReference.Draw(tex, destinationRectangle, sourceRectangle, Color.White);
                }
                // ○Texture ｘAnimator
                else if (ent.Texture != null && ent.Animator == null)
                {
                    Texture2D tex = Game1.spriteContent[ent.Texture.Sprite];
                    // Draw the texture without any animation
                    Game1.spriteBatchReference.Draw(tex, ent.C.Position, Color.White);
                }
            }
        }
    }
}
