﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace lyrical_engine_xna
{
    // ----------------------------------------------------------------------------------
    /*! \brief 
     * Deserializes entity_data, keeps a map of all entities, creates and destroys
     * instances of entities 
     * \n 
     * entity_dataをデシリアライズする、実体のマップを含む、実体のインスタンスを生成と削除します
     */
    // ----------------------------------------------------------------------------------
    public class EntityManager : IManager
    {
        /* ----------------------------------------------------------------------------------
         * INSTANCE VARIABLES
         * ---------------------------------------------------------------------------------- */
        // STATIC VARIABLES
        private static string m_entityData; // Data filepath for Entities
        private static Dictionary<int, Entity> m_entityMap; // Map of deserialized entities
        private static List<Entity> m_activeEntities; // Keep track of all currently relevant entities
        private static bool m_instantiated = false; // Assertion check
        private int m_idCounter = 0;

        // PROPERTIES
        public string EntityData /// Reference to the "entity_data" filepath \n 「entity_data」のファイルパスの変数
        {
            get { return EntityManager.m_entityData; }
        }

        public Dictionary<int, Entity> EntityMap /// Map of entities - key is ID, value is entities' data \n 実体のマップ - keyはID, valueは実体のデータ
        {
            get { return EntityManager.m_entityMap; }
        }

        public static List<Entity> ActiveEntities /// List of relevant Entities \n アクティブ実態のリスト
        {
            get { return m_activeEntities; }
        }

        public static bool Instantiated　/// Is this class already instantiated? \n このクラスはもうインスタンスを生成しましたか？
        {
            get { return EntityManager.m_instantiated; }
        }

        /* ----------------------------------------------------------------------------------
         * CONSTRUCTOR
         * ---------------------------------------------------------------------------------- */
        public EntityManager()
        {
            LyrEngDebugFunctions.Assert(!m_instantiated);
            m_entityData = System.IO.File.ReadAllText("data/entity_data");
            m_entityMap = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, Entity>>(m_entityData);
            m_activeEntities = new List<Entity>();
            m_instantiated = true;
        }

        /* ----------------------------------------------------------------------------------
         * FUNCTIONS
         * ---------------------------------------------------------------------------------- */

        /* ---------------------------------------------------------------------------------- */
        /*! \brief 
         * Instantiates a new entity based on values in EntityMap, then adds the new 
         * Entity to ActiveEntities. 
         * \n 
         * EntityMapから実体のインスタンスを生成する, そして
         * ActiveEntitiesにそのインスタンスを追加します。
         * 
         * We grab the entity by it's ID, then assign it a unique InstanceID. The new Entity
         * is added to ActiveEntities so that we can keep track of it, and run logic on it 
         * from other areas of the codebase.
         */
        /* ---------------------------------------------------------------------------------- */
        public void FetchEntity(int entID, Vector2 pos)
        {
            // Grab GameObject from map
            Entity ent = new Entity();

            // Assign unique instanceID to the entity
            ent.InstanceID = m_idCounter;
            m_idCounter++;

            // Assign new entity its component values based on value of entID
            AssignEntityComponentValues(m_entityMap[entID], ent);
            ent.C.Position = pos;

            // Add object to the active list
            m_activeEntities.Add(ent);
        }

        private void AssignEntityComponentValues(Entity baseEntity, Entity newEntity)
        {
            newEntity.C = baseEntity.C;
            newEntity.Texture = baseEntity.Texture;
            newEntity.Animator = baseEntity.Animator;
        }
    }
}
