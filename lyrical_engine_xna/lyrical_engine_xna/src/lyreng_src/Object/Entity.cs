﻿namespace lyrical_engine_xna
{
    // ----------------------------------------------------------------------------------
    /*! \brief 
     * Container for components - POD 
     * \n
     * コンポーネントのコンテナクラス － POD 
     */
    // ----------------------------------------------------------------------------------
    public class Entity
    {
        // Not component
        private int m_instanceID;

        public int InstanceID //!< uniqID
        {
            get { return m_instanceID; }
            set { m_instanceID = value; }
        }

        // Components
        public CommonComponent C { get; set; }
        public TextureComponent Texture { get; set; }
        public AnimatorComponent Animator { get; set; }
    }
}
