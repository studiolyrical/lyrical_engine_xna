﻿using Microsoft.Xna.Framework;
/* ----------------------------------------------------------------------------------
 * Description:
 * ● This is just a container for a bunch of interfaces. It also describes what each
 * interface does, so serves as a bit of architectural documentation.
 * ---------------------------------------------------------------------------------- */
namespace lyrical_engine_xna
{
    /* ----------------------------------------------------------------------------------
     * ● These classes run Update, FixedUpdate, or Draw.
     * ----------------------------------------------------------------------------------　 */
    public interface IUpdatable
    {
        // Functions
        void Update();
    }

    public interface IFixedUpdatable
    {
        // Functions
        void FixedUpdate();
    }

    public interface IDrawable
    {
        // Functions
        void Draw();
    }

    /* ----------------------------------------------------------------------------------
     * ● Manager is a role that exists somewhere inbetween a factory and a mediator,
     * specifically for managing Objects.
     * ● You go through Managers to fetch or remove objects.
     * ---------------------------------------------------------------------------------- */
    public interface IManager
    {
    }

    /* ----------------------------------------------------------------------------------
     * ● Systems maintain a list of components relevant to them and act on them. 
     * ● Components are purely data - the system is where the heavy lifting is done, using
     * that data. Managers act on Game Objects, Systems act on Components.
     * ---------------------------------------------------------------------------------- */
    public interface ISystem
    {
        // Functions
    }

    /* ----------------------------------------------------------------------------------
     * ● Components define objects. They are purely data, their corresponding logic would
     * exist in the appropriate System.
     * ● Usually a Component's values are deserialized from JSON.
     * ---------------------------------------------------------------------------------- */
    public interface IComponent
    {
    }

    // ----------------------------------------------------------------------------------
    /*! \brief
     * 
     */
    // ----------------------------------------------------------------------------------
    public interface ICommand
    {
        void Execute(Entity ent);
    }
}
