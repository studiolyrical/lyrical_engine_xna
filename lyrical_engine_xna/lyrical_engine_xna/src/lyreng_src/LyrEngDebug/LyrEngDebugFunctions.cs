﻿using System;
using System.Runtime.InteropServices;

namespace lyrical_engine_xna
{
    // ----------------------------------------------------------------------------------
    /*! \brief 
     * Static debug functions for LyricalEngine 
     * \n
     * LyricalEngineの静的デバッグ関数
     * 
     * ● Usually the functions in here are static and we just call them as we need them.
     */
    // ----------------------------------------------------------------------------------

    public class LyrEngDebugFunctions
    {
        // Grab user32.dll and hit up some message boxes
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern uint MessageBox(IntPtr hWnd, String text, String caption, uint type);

        // ----------------------------------------------------------------------------------
        /*! \brief Microsoft's Assert() method isn't aggressive enough for me, this one 
         * runs Debugger.Break() 
         * \n マイクロソフトのアサート関数はあまり侵略的じゃありません、この関数 はDebugger.Break()
         * を実行する
         */
        // ----------------------------------------------------------------------------------
        public static void Assert(bool c)
        {
            if (!c)
            {
                Console.WriteLine(System.Environment.StackTrace);
                MessageBox(new IntPtr(0), System.Environment.StackTrace, "♥リリカル☆マジカル ASSERT FAILED!♥", 0);
                System.Diagnostics.Debugger.Break();
                return;
            }
        }
    }
}
