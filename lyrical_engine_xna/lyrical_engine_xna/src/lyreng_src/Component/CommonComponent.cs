﻿using Microsoft.Xna.Framework;

namespace lyrical_engine_xna
{
    // ----------------------------------------------------------------------------------
    /*! \brief 
     * Universal data 
     * \n
     * 普遍的データ
     */ 
    // ----------------------------------------------------------------------------------
    public class CommonComponent : IComponent
    {
        public string Name { get; set; } //!< Entity's friendly name \n 実体の名前 
        public Vector2 Position { get; set; } //!< Entity's world coordinates \n 実体のワールド座標 
    }
}
