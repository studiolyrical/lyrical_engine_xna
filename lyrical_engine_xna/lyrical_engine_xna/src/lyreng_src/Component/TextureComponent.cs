﻿namespace lyrical_engine_xna
{
    // ----------------------------------------------------------------------------------
    /*! \brief 
     * Texture data 
     * \n 
     * テクスチャのデータ
     */ 
    // ----------------------------------------------------------------------------------
    public class TextureComponent
    {
        public string Sprite { get; set; } //!< Texture Atlas Filename (without extension) \n テクスチャアトラスのファイル名前(extなし)
        public int Columns { get; set; } //!< Total columns in the Atlas \n アトラスのカラムの総数
        public int Rows { get; set; } //!< Total rows in the Atlas \n アトラスの段の総数
    }
}
