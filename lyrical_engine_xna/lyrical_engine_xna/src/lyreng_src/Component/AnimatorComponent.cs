﻿namespace lyrical_engine_xna
{
    // ----------------------------------------------------------------------------------
    /*! \brief 
     * Animator Data 
     * \n
     * アニメーターのデータ 
     */
    //　----------------------------------------------------------------------------------

    public class AnimatorComponent : IComponent
    {
        public int TotalFrames { get; set; } //!< Total number of animation frames in the TextureAtlas \n テクスチャアトラスのコマの総数 
        public int CurrentFrame { get; set; } //!< The frame that the animator is currently on \n アニメーターの今のコマ 
        public float SampleRate { get; set; } //!< Framerate of the animation \n アニメーションのフレームレート 
        public float T { get; set; } //!< Loop value used by AnimationSystem() \n AnimationSystem()は使えるループ変数 
        public float Accumulator { get; set; } //!< Accumulator value used by AnimationSystem() \n AnimationSystem()は使えるの累算器変数 
    }
}
