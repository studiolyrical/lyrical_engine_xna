﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lyrical_engine_xna
{
    public class NullCommand : ICommand
    {
        public void Execute(Entity ent)
        {
            Console.WriteLine("null");
        }
    }

    public class SpawnCommand : ICommand
    {
        public void Execute(Entity ent)
        {
            Spawn();
        }

        private void Spawn()
        {
            Console.WriteLine("spawn");
            World.entManagerReference.FetchEntity(100, new Microsoft.Xna.Framework.Vector2(5.0f,5.0f));
        }
    }

    public class SpeakCommand : ICommand
    {
        public void Execute(Entity ent)
        {
            Speak(ent);
        }

        private void Speak(Entity ent)
        {
            Console.WriteLine("I'm " + ent + " aka " + ent.C.Name + " at: " + ent.C.Position);
        }
    }

    public class ListEntitiesCommand : ICommand
    {
        public void Execute(Entity ent)
        {
            List();
        }

        private void List()
        {
            Console.WriteLine("list");
            foreach (Entity ent in EntityManager.ActiveEntities)
            {
                Console.WriteLine(ent.InstanceID);
            }
        }
    }
}
