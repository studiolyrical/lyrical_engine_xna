﻿using Microsoft.Xna.Framework;

namespace lyrical_engine_xna
{
    // ----------------------------------------------------------------------------------
    /*! \brief Class for game camera 
     * \n ゲームカメラのクラス
     * 
     * \todo ● Right now this class just holds the camera variable, doing battle 
     * prototyping so no logic at all. When it comes time to write overworld, will 
     * need to add proper tracking logic.
     */
    // ----------------------------------------------------------------------------------
    public class Camera
    {
        private Vector2 m_location;

        public Vector2 Location //!< Camera's coordinates \n カメラの座標
        {
            get { return m_location; }
            set { m_location = value; }
        }
    }
}
