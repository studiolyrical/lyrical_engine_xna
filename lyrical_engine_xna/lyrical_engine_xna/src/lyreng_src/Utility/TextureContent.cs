﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace lyrical_engine_xna
{
    // ----------------------------------------------------------------------------------
    /*! \brief 
     * Loads all content in a directory 
     * \n 
     * 一つディレクトリのコンテントをロードする 
     * 
     * \todo ● This class is temporary and will need reworking for actual game projects
     */
    // ----------------------------------------------------------------------------------
    public static class TextureContent
    {
        public static Dictionary<string, T> LoadListContent<T>(this ContentManager contentManager, string contentFolder)
        {
            DirectoryInfo dir = new DirectoryInfo(contentManager.RootDirectory + "/" + contentFolder);
            if (!dir.Exists)
                throw new DirectoryNotFoundException();
            Dictionary<String, T> result = new Dictionary<String, T>();

            FileInfo[] files = dir.GetFiles("*.*");
            foreach (FileInfo file in files)
            {
                string key = Path.GetFileNameWithoutExtension(file.Name);


                result[key] = contentManager.Load<T>(contentFolder + "/" + key);
            }
            return result;
        }
    }
}
