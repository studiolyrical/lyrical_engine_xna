﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.IO;
using System.Collections.Generic;

namespace lyrical_engine_xna
{

    // ----------------------------------------------------------------------------------
    /*! \brief 
     * Evil utility class containing static functions for various operations. Ufufuっ!
     * \n 
     * 悪のユーティリティクラス、いろいろな静的関数を含む。うふふっ！
     */
    // ----------------------------------------------------------------------------------
    public class HelperFunctions
    {
        // ----------------------------------------------------------------------------------
        /*! \brief Take a string that is formatted like a Vector2 (intX,intY), convert it to 
         * a real Vector2, and return 
         * \n Vector2(intX,intY)の形式stringは本物のVector2を変わる、そして戻り値を返す
         */
        // ----------------------------------------------------------------------------------
        public static Vector2 StringToVector2(string s)
        {
            string[] splitter = s.Substring(0, s.Length - 1).Split(',');
            float x = float.Parse(splitter[0]);
            float y = float.Parse(splitter[1]);
            Vector2 vector2 = new Vector2(x, y);
            return vector2;
        }
    }
}
